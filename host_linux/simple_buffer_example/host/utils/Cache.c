/*
 *  @file   Cache.c
 *
 *  @brief      Cache API implementation
 *
 *
 *  ============================================================================
 *
 *  Copyright (c) 2008-2017, Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *  
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  Contact information for paper mail:
 *  Texas Instruments
 *  Post Office Box 655303
 *  Dallas, Texas 75265
 *  Contact information: 
 *  http://www-k.ext.ti.com/sc/technical-support/product-information-centers.htm?
 *  DCMP=TIHomeTracking&HQS=Other+OT+home_d_contact
 *  ============================================================================
 *  
 */

/* Standard headers */
#include <ti/ipc/Std.h>

#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdio.h>

#include <linux/dma-buf.h>

#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */

extern int dma_buf_fd;

/*
 *  ======== Cache_inv ========
 */
Void Cache_inv(Ptr blockPtr, UInt32 byteCnt, Bits16 type, Bool wait)
{
    /* Do cache inv using CMEM device */
    struct dma_buf_sync sync_start = {0};
    sync_start.flags = DMA_BUF_SYNC_START | DMA_BUF_SYNC_RW;
    int rv = ioctl(dma_buf_fd, DMA_BUF_IOCTL_SYNC, &sync_start);
    if (rv < 0)
            printf("Failed DMA_BUF_SYNC_START: %s\n", strerror(errno));
}

/*
 *  ======== Cache_wb ========
 */
Void Cache_wb(Ptr blockPtr, UInt32 byteCnt, Bits16 type, Bool wait)
{
    /* Do cache writeback using CMEM device */
    struct dma_buf_sync sync_end = {0};
    sync_end.flags = DMA_BUF_SYNC_END | DMA_BUF_SYNC_RW;
    int rv = ioctl(dma_buf_fd, DMA_BUF_IOCTL_SYNC, &sync_end);
    if (rv < 0)
              printf("Failed DMA_BUF_SYNC_END: %s\n", strerror(errno));
}

/*
 *  ======== Cache_wbInv ========
 */
Void Cache_wbInv(Ptr blockPtr, UInt32 byteCnt, Bits16 type, Bool wait)
{
    /* Do cache write back and inv using CMEM device */
    Cache_wb(blockPtr, byteCnt, type, wait);
    Cache_inv(blockPtr, byteCnt, type, wait);
}

#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */
